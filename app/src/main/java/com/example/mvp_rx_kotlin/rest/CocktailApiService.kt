package com.example.mvp_rx_kotlin.rest

import com.example.mvp_rx_kotlin.model.DrinksData
import io.reactivex.Observable
import retrofit2.http.GET

interface CocktailApiService {
    @GET("json/v1/1/random.php")
    fun getRandomDrink(): Observable<DrinksData>
}