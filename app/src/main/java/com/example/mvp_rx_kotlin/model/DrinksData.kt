package com.example.mvp_rx_kotlin.model

import com.google.gson.annotations.SerializedName

data class DrinksData(
    @SerializedName("drinks") var drinks: List<DrinkData>
)

data class DrinkData(
    @SerializedName("idDrink") var id: String,
    @SerializedName("strDrink") var name: String,
    @SerializedName("strCategory") var category: String,
    @SerializedName("strAlcoholic") var alcoholic: String,
    @SerializedName("strGlass") var glass: String,
    @SerializedName("strInstructions") var instructions: String,
    @SerializedName("strDrinkThumb") var thumb: String
)