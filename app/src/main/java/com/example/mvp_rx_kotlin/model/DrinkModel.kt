package com.example.mvp_rx_kotlin.model

import com.example.mvp_rx_kotlin.BuildConfig
import com.example.mvp_rx_kotlin.rest.CocktailApiService
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class DrinkModel {
    fun getRandomDrink(): Observable<DrinksData> {
        return Retrofit.Builder().baseUrl(BuildConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CocktailApiService::class.java)
            .getRandomDrink()
    }

}

