package com.example.mvp_rx_kotlin.presenter

import com.example.mvp_rx_kotlin.model.DrinkData
import com.example.mvp_rx_kotlin.model.DrinkModel
import com.example.mvp_rx_kotlin.model.DrinksData
import com.example.mvp_rx_kotlin.view.MainActivity
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DrinkPresenter(val view: MainActivity) {
    fun getDrink() {
        view.showProgressBar()
        DrinkModel()
            .getRandomDrink()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DrinksData> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: DrinksData) {
                view.updateDrinkView(t.drinks[0])
                view.hideProgressBar()
            }

            override fun onError(e: Throwable) {
            }
        })
    }

    fun saveDrink(){

    }

    interface MainActivityView {
        fun updateDrinkView(drink: DrinkData)
        fun showProgressBar()
        fun hideProgressBar()
    }
}