package com.example.mvp_rx_kotlin.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.mvp_rx_kotlin.R
import com.example.mvp_rx_kotlin.model.DrinkData
import com.example.mvp_rx_kotlin.presenter.DrinkPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DrinkPresenter.MainActivityView {
    lateinit var presenter: DrinkPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = DrinkPresenter(this)

        button.setOnClickListener { presenter.getDrink() }
    }

    override fun updateDrinkView(drink: DrinkData) {
        Picasso.get().load(drink.thumb).into(thumb)
        name.text = drink.name
    }

    override fun showProgressBar() {
        progress_bar.visibility = View.VISIBLE
        thumb.visibility = View.GONE
    }

    override fun hideProgressBar() {
        progress_bar.visibility = View.GONE
        thumb.visibility = View.VISIBLE
    }
}
